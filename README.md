# ELK Perceval

Analyzing jitsi git data using Perceval Grimoirelab, Elasticsearch and Kibana. [This blog post](https://dev.to/anajsana95/how-to-measure-covid-19-impact-on-jitsi-project-with-python-and-elasticsearch-154j) sumarizes main concepts of the analysis.

## Before you start

When runing `jitsi-data.py` script, make sure to have:

1. ElasticSearch instance running
2. Kibana running
3. Perceval instaled (pip install perceval)
4. Mapping.json file

## How to get the data and visualization

Before running the script, we recomend you to have the same ElasticSEarch index name as the index pattern name used for `impact_dashboard.dnjson`. Make sure to change "repo_jitsi_index_def" to "repo_jitsi_index_final"

``` 
index_name = "repo_jitsi_index_def"
```

Gathering and data storage process will take a few minutes (you can use visual studio code or pycharm to run the script). Once done, you should see in your ElasticSearch and Kibana your index.

After this, simple download `impact_dashboard.dnjson` file and import it in your Kibana. You should go to `Manage > Saved objects` and click on **Import**. Once imported, you can connect your stored data to **impact_dashboard** dashboard and Kibana index pattern

If you have any questions or would like to contirbute, please submit an issue :) 
